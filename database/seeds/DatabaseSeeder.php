<?php

use Illuminate\Database\Seeder;
use App\Rol;
use App\User;
use App\Category;
use App\DiscountVouncher;
use App\Domiciliary;
use App\PaymentMethod;
use App\Product;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UserSeeder::class);

        Rol::create([
    		"name" => "CLIENTE"
        ])->save();
        Rol::create([
    		"name" => "ADMIN"
        ])->save();
        Rol::create([
        "name" => "DOMICILIARIO"
        ])->save();
        
          User::create([
            "name" => "Luis Domicilio",
            "email" => "luis.domicilio.v@hotmail.com",
            "phone" => "3017468682",
            "direction" => "calle 40 #15-40",
            "password" => bcrypt('secret'),            
            "state" => 'ACTIVO',
            "rol_id" => "3",
          ])->save();
          User::create([
            "name" => "Luis Tigo",
            "email" => "luis.angel.v@hotmail.com",
            "phone" => "3017468680",
            "direction" => "calle 40 #15-40",
            "password" => bcrypt('secret'),            
            "state" => 'ACTIVO',
            "rol_id" => "1",
        ])->save();

        User::create([
            "name" => "Luis Vega",
            "email" => "luisprofee@gmail.com",
            "phone" => "3017468680",
            "direction" => "calle 40 #15-40",
            "password" => bcrypt('secret'),            
            "state" => 'ACTIVO',
            "rol_id" => "2",
        ])->save();
        
        Category::create([
          "name" => "Licores",
          "description" => "description description description",
        ])->save();

      Category::create([
        "name" => "Carnes Frias",
        "description" => "description description description",
      ])->save();

      Product::create([
        "name" => "Old Pard",
        "quantity" => "10",
        "picture" => "",
        "title" => "Whisky",
        "description" => "envio gratis",
        "category_id" => "1"
      ])->save();

      Product::create([
        "name" => "Whisky buchanans",
        "quantity" => "10",
        "picture" => "",
        "title" => "Whisky",
        "description" => "envio gratis",
        "category_id" => "1"
      ])->save();

      Product::create([
        "name" => "Chorizo cuni",
        "quantity" => "25",
        "picture" => "",
        "title" => "Comida rapida",
        "description" => "envio gratis",
        "category_id" => "2"
      ])->save();

      Product::create([
        "name" => "Salchicha ranchera",
        "quantity" => "18",
        "picture" => "",
        "title" => "Comida rapida",
        "description" => "envio gratis",
        "category_id" => "2"
      ])->save();
      DiscountVouncher::create([
        "code"=> "",
        "price"=> "0",
        "state"=> "1"
      ])->save();
      DiscountVouncher::create([
        "code"=> "teloconsigo2020",
        "price"=> "3500",
        "state"=> "1"
      ])->save();
      DiscountVouncher::create([
        "code"=> "teloconsigo2019",
        "price"=> "2500",
        "state"=> "1"
      ])->save();


      //domiciliario
    Domiciliary::create([
        "name" => "Domicilio 1",
        "phone" => "3017468689",
        'state' => 1
    ])->save();
    Domiciliary::create([
        "name" => "Domicilio 2",
        "phone" => "3017468689",
        'state' => 1
    ])->save();

    PaymentMethod::create([
        "name" => "Efectivo",
    ])->save();
    PaymentMethod::create([
        "name" => "Datafono",
    ])->save();


    $this->call(DepartmentsTableSeeder::class);
    
    }


    

}
