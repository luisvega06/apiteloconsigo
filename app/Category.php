<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    protected $fillable = [
        'name', 
        'description',
        'state'
    ];

    public function products(){
        return $this->hasMany('App\Product');
    }

    public function users(){
        return $this->hasMany('App\User');
    }
}
