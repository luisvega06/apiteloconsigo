<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PaymentMethod extends Model
{
    protected $fillable = [
        'name'
    ];

    public function products(){
        return $this->hasMany('App\Product');
    }
    public function order(){
        return $this->belongsTo('App\Order');
    }
}
