<?php

namespace App;

use DateTime;
use Exception;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Order extends Model
{
    protected $fillable = [
        "user_id",
        "domiciliary_id",
        "discount_vouncher_id",
        "payment_method_id",
        "total",
        "state"
    ];

    public function product(){
        return $this->belongsTo('App\Product');
    }
    public function user(){
        return $this->belongsTo('App\User');
    }
    public function domiciliary(){
        return $this->belongsTo('App\User', 'domiciliary_id', 'id');
    }
    public function reservations(){
        return $this->hasMany('App\Reservation');
    }
    
    public function discountVouncher(){
        return $this->belongsTo('App\DiscountVouncher');
    }
    public function paymentMethod(){
        return $this->belongsTo('App\PaymentMethod');
    }

    public static function productInSales($stateOrder=2, $dateRange="this mount", $date=null){
        $products = array();
        if($dateRange == "this mount"){
            $now = new DateTime();
            $datas = Order::with(['reservations'=> function($query){
                $query->with(['product'=> function($q){
                    $q->with('category');
                }]);
            }])
            ->where('orders.state', $stateOrder)
            ->whereMonth('created_at', $now->format('m'))
            ->get();    
        }else if($date != null){
            $datas = Order::with(['reservations'=> function($query){
                $query->with(['product'=> function($q){
                    $q->with('category');
                }]);
            }])
            ->whereDate('orders.created_at',$date)
            ->where('orders.state', $stateOrder)
            ->get();
        }else{
            $datas = Order::with(['reservations'=> function($query){
                $query->with(['product'=> function($q){
                    $q->with('category');
                }]);
            }])
            ->where('orders.state', $stateOrder)
            ->get();
        }
        
        foreach($datas as $data){
            foreach($data['reservations'] as $x){
                array_push($products, [
                        'id' => $x['product']['id'],
                        'name' => $x['product']['name'],
                        'price' => $x['product']['price'],
                        'quantity' => $x['quantity'],
                        'picture' => $x['product']['picture'],
                        'category_id' => $x['product']['category']['id'],
                        'category' => $x['product']['category']['name'],
                        'created_at' => $data['created_at'],
                        'updated_at' => $data['updated_at']
                    ]);
            }
        }        
        foreach ($products as $key => $row) {
            $aux[$key] = $row['quantity'];
        }
        try{
            array_multisort($aux, SORT_DESC, $products);

        }catch(Exception $e){

        }


        return $products;
    }

    public static function getSales($type, $startDate, $endDate, $rowNum=0){
        $tipo = "";
        
        if($type == "valor"){

        }else if($type == "cantidad"){

        }
        $data = Order::with(['reservations'=> function($query){
            $query->with('product');
        }])
        ->where('')
        ->orderBy("id", "desc")
        ->take(3)
        ;


    }
    public static function getTopSales($order, $rowNum){

    }

    public static function getSalesQuantityByDay($date){
        $data = Order::productInSales(2, "this mounte", $date);
        return $data;
    }
    
    public static function getSale($date){
        $data = Order::where('state', 2)
        ->select(
            DB::raw("DATE_FORMAT(created_at,'%Y-%m-%d') as date"), 
            DB::raw('SUM(total) as value')
            )
        ->whereDate('created_at','=', $date)
        ->get();
        if($data==null){
            return 0;
        }
        $dataFinal = array();
        foreach($data as $x){
            if($x)
            array_push($dataFinal, ["value"=>$x->value]);
            else
            array_push($dataFinal, ["value"=>0]);
            
                
        }
        return $dataFinal;
    }

    public function reportOrders(){
        $orders = Order::with('user')->where('state', 2);
        return response()->json(["orders" => $orders]);
    }
}
