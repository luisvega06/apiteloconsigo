<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Domiciliary extends Model
{
    protected $fillable = [
        'name',
        'phone',
        'state'
    ];

    public function orders(){
        return $this->hasMany('App\Order', 'domiciliary_id', 'id');
    }

        
    
 
    
}
