<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Reservation extends Model
{
    protected $fillable = [
        'user_id', 
        'product_id',
        'quantity',
        'state',
        'order_id'
        
    ];

    public function product(){
        return $this->belongsTo('App\Product');
    }
    public function user(){
        return $this->belongsTo('App\User');
    }
    public function order(){
        return $this->belongsTo('App\Order');
    }
}
