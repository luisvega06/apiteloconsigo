<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Municipality extends Model
{
    protected $fillable = [
        'municipality',
        'department_id'
    ];

    public function department()
    {
        return $this->belongsTo('App\Deparment');
    }
}
