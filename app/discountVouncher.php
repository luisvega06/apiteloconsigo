<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DiscountVouncher extends Model
{
    protected $fillable = [
        'code',
        'price',
        'state'
    ];

    public function order(){
        return $this->belongsTo('App\Order');
    }

    
}
