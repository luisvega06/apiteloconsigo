<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\user;
use App\Rol;    

class UserController extends Controller
{

  
    public function index()
    {
        $users = User::all();
        return response()->json(["users"=>$users]);
    }

    public function store(Request $request)
    {
        $userAux = User::where('email', $request->email)->get();
        if(count($userAux)<=0){
            $user = User::create([
                'name' => $request->name, 
                'email' => $request->email, 
                'phone' => $request->phone, 
                'direction' => $request->direction,  
                'password' => bcrypt($request->password), 
                'state' => $request->state, 
                'rol_id' => $request->rol_id,
                'department_id' => $request->department_id,
                'municipality_id' => $request->municipality_id,
                'user_id' => $request->user_id,
                'category_id' => $request->category_id
                
                
                
            ]);
        }
        else{
            return response()->json(["user"=>"ya existe"]);
        }
 
        return response()->json(["user"=>$user]);
    }

    public function show($id)
    {
        $user = User::find($id);
        return response()->json(["user"=>$user]);
    }

   
    public function update(Request $request, $id)
    {
        $user = User::find($id);
        $user->update($request->input());
        return response()->json(["user"=>$user]);
    }

    public function destroy($id)
    {
        try {
            $user = User::findOrFail($id);
            $user->delete();
            return response()->json(["msj"=>"el usuario fue eliminado con exito"]);
        } catch (\Exception $exception) {
            return response()->json(["msj"=>"No existe el usuario que quiere eliminar"]);   
        }
    }

    public function getUsers($id){
        $advisers = User::with(['users' => function($query){            
            $query->with(['category' => function ($query2){
                $query2->select('id', 'name')->get();
            }])
            ->get();
        }])->select([
            'id',
            'name'
        ])->where('id', $id)->get();
        
        

        return $advisers;
    }

    public function getLeaders(){

        $rol_id = !Rol::where('name', 'LIDER')->get()->isEmpty() ? Rol::where('name', 'LIDER')->first()->id : 0;
        $leaders = [];
        if($rol_id > 0)
            $leaders = User::select('id', 'name')->where('rol_id', $rol_id)->get();
        
        return $leaders;        
    }
}


