<?php

namespace App\Http\Controllers;

use App\Domiciliary;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class DomiciliaryController extends Controller
{

    public function index()
    { 
        $domiciliary = array();
        $domiciliaries = User::with(['ordersDomiciliary'=>function($query){
            $query->where('state', 2);
        }])
        ->where('id', '!=', 1)
        ->where('state', '=', 1)
        ->where('rol_id', 3)
        ->get();
        foreach($domiciliaries as $x){
            
            array_push($domiciliary, [
                'domiciliaryId'         => $x->id, 
                'domiciliaryName'       => $x->name, 
                'domiciliaryPhone'      => $x->phone,
                'domiciliaryCreated_at' => $x->created_at->format('d/m/Y'), 
                'orders'                => count($x->orders)
                ]);
        }  
        return response()->json(["domiciliary"=>$domiciliary]);
    }
  
    public function store(Request $request)
    {
        $domiciliary = User::create($request->input());
        return response()->json(["domiciliary"=>$domiciliary]);
    }
    
    public function actualizar(Request $request, $id, $state)
    {
        $domiciliary = User::find($id);
        $domiciliary->state = $state;
        $domiciliary->save();
        return response()->json(["domiciliary"=>$domiciliary]);
    }

    public function eliminar($id)
    {
        try {
            $domiciliary = User::findOrFail($id);
            $domiciliary->state = 0;
            return response()->json(["msj"=>"el domicilio fue eliminado del mostrador con exito"]);
        } catch (\Exception $exception) {
            return response()->json(["msj"=>"No existe el domicilio que quiere eliminar"]);   
        }
    }

    public function getOrders($state){
        $data = array();
        $domiciliaries = User::with(['ordersDomiciliary'=>function($query) use ($state){
            $query->where('state', '=' , $state)->get();
        }])
        ->where('id', '!=', 1)
        ->where('rol_id', '=', 3)
        
        ->get();

        foreach($domiciliaries as $domiciliary){
            
            array_push($data, [
                'domiciliaryId' => $domiciliary->id,
                'domiciliaryName' => $domiciliary->name, 
                'domiciliaryPhone' => $domiciliary->phone,
                'domiciliaryState' => $domiciliary->state,
                'domiciliaryCreated_at' => $domiciliary->created_at->format('d/m/Y'), 
                'orders'      => count($domiciliary->ordersDomiciliary)
                ]);
        }        
        return response()->json(["domiciliaries"=>$data]);
    }

    public function getMyOrders($id, $state){

        $response = array();
        
        /*$orders = User::with(['ordersDomiciliary'=>function($query) use ($state){
            $query->with('user')
            ->where('state', 0);
        }])
        ->where('id', '=', $id)
        ->get();
        
        return response()->json(["orders"=>$orders]); */
        
        //

        $data = DB::table('orders')
                            ->join('users', 'orders.user_id', 'users.id')
                            ->select(
                                'users.id as userId',
                                'users.name as userName',
                                'orders.id as orderId',
                                'orders.created_at as orderCreated_at',
                                'orders.state as orderState'
                                )
                            ->where('orders.state', '=', $state)
                            ->where('domiciliary_id', $id)
                            ->get();
        return response()->json(["orders"=>$data]);
    }
    
}
