<?php

namespace App\Http\Controllers;

use App\DiscountVouncher;
use Illuminate\Http\Request;

class DiscountVouncherController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $discountVouncher = DiscountVouncher::all();
        return response()->json(["discountVouncher"=>$discountVouncher]);
    }
    public function __construct()
    {
        $this->middleware('jwt.auth');
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $discountVouncher = DiscountVouncher::create($request->input());
        return response()->json(["discountVouncher"=>$discountVouncher]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\discountVouncher  $discountVouncher
     * @return \Illuminate\Http\Response
     */
    public function show(discountVouncher $discountVouncher)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\discountVouncher  $discountVouncher
     * @return \Illuminate\Http\Response
     */
    public function edit(discountVouncher $discountVouncher)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\discountVouncher  $discountVouncher
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $discountVouncher = DiscountVouncher::find($id);
        $discountVouncher->update($request->input());
        return response()->json(["discountVouncher"=>$discountVouncher]);
    }
    public function actualizar(Request $request, $id, $state)
    {
        $discountVouncher = DiscountVouncher::find($id);
        $discountVouncher->state = $state;
        $discountVouncher->save();
        return response()->json(["discountVouncher"=>$discountVouncher]);
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\discountVouncher  $discountVouncher
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $discountVouncher = DiscountVouncher::findOrFail($id);
            $discountVouncher->state = 0;
            return response()->json(["msj"=>"el bono de descuento fue eliminado del mostrador con exito"]);
        } catch (\Exception $exception) {
            return response()->json(["msj"=>"No existe el bono de descuento que quiere eliminar"]);   
        }
    } 

    public function eliminar($id)
    {
        try {
            $discountVouncher = DiscountVouncher::findOrFail($id);
            $discountVouncher->state = 0;
            return response()->json(["msj"=>"el bono de descuento fue eliminado del mostrador con exito"]);
        } catch (\Exception $exception) {
            return response()->json(["msj"=>"No existe el bono de descuento que quiere eliminar"]);   
        }
    }
}
