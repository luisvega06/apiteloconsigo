<?php

namespace App\Http\Controllers;

use App\Order;
use App\Reservation;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ReservationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('jwt.auth');
    }
    public function index()
    {
        $reservations = DB::table('reservations')
                            ->join('products', 'reservations.product_id', 'products.id')
                            ->join('users', 'reservations.user_id', 'users.id')
                            ->where('reservations.state', '=', 0)
                            ->get();
        return response()->json(["reservations"=>$reservations]);
        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $reservation = Reservation::where('product_id', $request->product_id)->where('user_id', $request->user_id)->where('state', 0)->get();
        if(count($reservation)>0){
            return response()->json(["message"=>"reserva existe"]);
        }else{
            $reservation = Reservation::create($request->input());
            return response()->json(["reservation"=>$reservation]);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Reservation  $reservation
     * @return \Illuminate\Http\Response
     */
    public function show(Reservation $reservation)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Reservation  $reservation
     * @return \Illuminate\Http\Response
     */
    public function edit(Reservation $reservation)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Reservation  $reservation
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $reservation = Reservation::find($id);
        $reservation->update($request->input());
        return response()->json(["reserva$reservation"=>$reservation]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Reservation  $reservation
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $reservation = Reservation::findOrFail($id);
            $reservation->state = 0;
            return response()->json(["msj"=>"La reserva fue eliminada del mostrador con exito"]);
        } catch (\Exception $exception) {
            return response()->json(["msj"=>"No existe la reserva que quiere eliminar"]);   
        }
    }
    public function eliminar($id)
    {
        try {
            $reservation = Reservation::findOrFail($id);
            $reservation->delete();
            return response()->json(["msj"=>"La reserva fue eliminada del mostrador con exito"]);
        } catch (\Exception $exception) {
            return response()->json(["msj"=>"No existe la reserva que quiere eliminar"]);   
        }
    }

    public function getMyReservation($id){
        $reservations = DB::table('reservations')
                            ->join('products', 'reservations.product_id', 'products.id')
                            ->join('users', 'reservations.user_id', 'users.id')
                            ->select(
                                'users.id as userId',
                                'reservations.id as reservationId',
                                'products.id as productId',
                                'reservations.quantity as reservationQuantity',
                                'products.name as productName',
                                'products.price as productPrice',
                                'products.quantity as productQuantity',
                                DB::raw('(reservations.quantity * products.price) as reservationValueTotal')
                                )
                            ->where('reservations.state', '=', 0)
                            ->where('user_id', $id)
                            ->get();
        
        return response()->json(["reservations"=>$reservations]);
    }
    public function updateQuantity(Request $request, $reservation_id, $quantity){
        $reservation = Reservation::findOrFail($reservation_id);
        $reservation->quantity = $quantity;
        $reservation->save();
        return response()->json(["reservation"=>$reservation]);

    }
    public function updateState($id, $state){
        $reservation = Reservation::findOrFail($id);
        $reservation->state = $state;
        $reservation->save();
        return response()->json(["reservation"=>$reservation]);   


    } 

    public function confirmarPedido(Request $request){
        
        

         
        try{
            $order = Order::create([
                'state' => 0,
                'user_id' => $request->all()[0][0]['userId'],
                'domiciliary_id' => $request->all()[1][0]['domiciliary_id'],
                'discount_vouncher_id' => $request->all()[1][0]['discount_vouncher_id'],

                'payment_method_id' => $request->all()[1][0]['payment_method_id'],
                'total' => $request->all()[1][0]['total'],
                'created_at' => date("Y-m-d H:i:s"),
                'updated_at' => date("Y-m-d H:i:s")

            ]);
        }catch(Exception $e){

        }
        
        foreach($request->all()[0] as $x){
            $reservation = Reservation::findOrFail($x['reservationId']);
            $reservation->state = 1;
            $reservation->order_id = $order->id;
            $reservation->save();
        }
       
        
        return response()->json(["order"=>$order]);   
        

    }
    public function getMyReservations($id)
    {
        
        $reservations = DB::table('reservations')
                            ->join('products', 'reservations.product_id', 'products.id')
                            ->join('users', 'reservations.user_id', 'users.id')
                            ->where('reservations.state', '=', 1)
                            ->select(
                                'users.id as userId',
                                'reservations.id as reservationId',
                                'products.id as productId',
                                'reservations.quantity as reservationQuantity',
                                'products.name as productName',
                                'products.price as productPrice',
                                DB::raw('(reservations.quantity * products.price) as reservationValueTotal')
                                )
                            ->where('user_id', $id)
                            ->get();
        
        return response()->json(["reservations"=>$reservations]);
        
    }
}
