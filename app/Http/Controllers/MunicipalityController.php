<?php

namespace App\Http\Controllers;

use App\Municipality;
use Illuminate\Http\Request;

class MunicipalityController extends Controller
{
    public function getMunicipality($department_id){
        $municipality = Municipality::where('department_id', $department_id)->get();
        return response()->json(['municipality' => $municipality]);
    }
}
