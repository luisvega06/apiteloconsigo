<?php

namespace App\Http\Controllers;

use App\Publication;
use DB;
use Illuminate\Http\Request;

class PublicationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('jwt.auth');
    }
    
    public function index()
    {
        $publications = Publication::where('state', '=', 1)->get();
        $publications = DB::table('publications')
                                ->join('products', 'publications.product_id', 'products.id')
                                ->join('users', 'publications.user_id', 'users.id')
                                ->join('categories', 'products.category_id', 'categories.id')
                                ->select(
                                    'publications.state as publicationState',
                                    'publications.title as publicationTitle',
                                    'publications.id as publicationId',
                                    'publications.description as publicationDescription',
                                    'publications.photo as publicationPhoto',
                                    'publications.created_at as publicationCreated_at',
                                    'products.id as productId',
                                    'products.name as productName',
                                    'products.quantity as productCuantity',
                                    'products.price as productPrice',
                                    'products.offer as productOffer',
                                    'products.picture as productPicture',
                                    'categories.name as categoryName',
                                    'users.name as userName'
                                    )
                                ->where('publications.state', '=', 1)
                            ->orderBy('publications.created_at', 'desc')

                            ->get();
        return response()->json(["publications"=>$publications]);
    }

    public function indexAdmin(){
        $publications = Publication::all();
        $publications = DB::table('publications')
                                ->join('products', 'publications.product_id', 'products.id')
                                ->join('users', 'publications.user_id', 'users.id')
                                ->join('categories', 'products.category_id', 'categories.id')
                                ->select(
                                    'publications.state as publicationState',
                                    'publications.title as publicationTitle',
                                    'publications.id as publicationId',
                                    'publications.description as publicationDescription',
                                    'publications.photo as publicationPhoto',
                                    'publications.created_at as publicationCreated_at',
                                    'products.id as productId',
                                    'products.name as productName',
                                    'products.quantity as productCuantity',
                                    'products.price as productPrice',
                                    'products.offer as productOffer',
                                    'products.picture as productPicture',
                                    'categories.name as categoryName',
                                    'users.name as userName'
                                    )
                                
                            ->orderBy('publications.created_at', 'desc')

                            ->get();
        return response()->json(["publications"=>$publications]);
    }

    public function getPublicationByState($state)
    {
        $publications = Publication::where('state', '=', 1)->get();
        $publications = DB::table('publications')
                                ->join('products', 'publications.product_id', 'products.id')
                                ->join('users', 'publications.user_id', 'users.id')
                                ->join('categories', 'products.category_id', 'categories.id')
                                ->select(
                                    'publications.title as publicationTitle',
                                    'publications.id as publicationId',
                                    'publications.description as publicationDescription',
                                    'publications.photo as publicationPhoto',
                                    'publications.created_at as publicationCreated_at',
                                    'products.id as productId',
                                    'products.name as productName',
                                    'products.quantity as productCuantity',
                                    'products.price as productPrice',
                                    'products.offer as productOffer',
                                    'products.picture as productPicture',
                                    'categories.name as categoryName',
                                    'users.name as userName'
                                    )
                                ->where('publications.state', '=', $state)
                            ->get();
        return response()->json(["publications"=>$publications]);
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $publication = Publication::create($request->input());
        return response()->json(["publication"=>$publication]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Publication  $publication
     * @return \Illuminate\Http\Response
     */
    public function show(Publication $publication)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Publication  $publication
     * @return \Illuminate\Http\Response
     */
    public function edit(Publication $publication)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Publication  $publication
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $publication = Publication::find($id);
        $publication->update($request->input());
        return response()->json(["publication"=>$publication]);
    }
    public function updateState($id, $state){
        $order = Publication::findOrFail($id);
        $order->state = $state;
        $order->save();
        return response()->json(["order"=>$order]);   
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Publication  $publication
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $publication = Publication::findOrFail($id);
            $publication->state = 0;
            return response()->json(["msj"=>"La publicación fue eliminada del mostrador con exito"]);
        } catch (\Exception $exception) {
            return response()->json(["msj"=>"No existe la publicacion que quiere eliminar"]);   
        }
    }
}
