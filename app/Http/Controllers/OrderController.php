<?php

namespace App\Http\Controllers;

use App\Order;
use App\Product;
use Carbon\Carbon;
use DateInterval;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class OrderController extends Controller
{
    /** 
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    
    public function index()
    {
        $orders = DB::table('orders')
                            ->join('users', 'orders.user_id', 'users.id')
                            ->select(
                                'users.id as userId',
                                'users.name as userName',
                                'orders.id as orderId',
                                'orders.created_at as orderCreated_at',
                                'orders.state as orderState'
                                )
                            ->where('orders.state', '=', 0)
                            ->orderBy('orders.created_at', 'desc')
                            
                            ->get();
        return response()->json(["orders"=>$orders]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function getOrdersUser($id)
    {
        $orders = DB::table('orders')
                            ->join('users', 'orders.user_id', 'users.id')
                            ->join('products', 'orders.user_id', 'products.id')
                            ->select(
                                'users.id as userId',
                                'users.name as userName',
                                'orders.id as orderId',
                                'orders.created_at as orderCreated_at',
                                'orders.state as orderState',
                                'orders.quantity as orderQuantity',
                                'products.id as productId',
                                'products.name as productName',
                                'products.price as productPrice',
                                'products.offer as productOffer'
                                
                                )
                            ->where('orders.state', '=', 0)
                            ->where('orders.id', '=', $id)
                            ->get();
        return response()->json(["orders"=>$orders]);    
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $order = Order::where('product_id', $request->product_id)->where('user_id', $request->user_id)->get();
        if($order>0){
            return response()->json(["message"=>"reserva existe"]);
        }else{
            $order = order::create($request->input());
            return response()->json(["order"=>$order]);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Order  $order
     * @return \Illuminate\Http\Response
     */
    public function show(Order $order)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Order  $order
     * @return \Illuminate\Http\Response
     */
    public function edit(Order $order)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Order  $order
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $order = Order::find($id);
        $order->update($request->input());
        return response()->json(["order"=>$order]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Order  $order
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $order = Order::findOrFail($id);
            $order->state = 0;
            return response()->json(["msj"=>"El pedido fue eliminado con exito"]);
        } catch (\Exception $exception) {
            return response()->json(["msj"=>"No existe el pedido que quiere eliminar"]);   
        }
    }

    public function updateState($id, $state){
        $order = Order::findOrFail($id);
        $order->state = $state;
        $order->save();
        return response()->json(["order"=>$order]);   
    }
    public function getOrdersState($state){
        //si quieres buscar los que están pendiente pasa un 0, si quieres bucar los que se han confirmado envia un 1, y para los que ya se han
        //entregado manda un 2
        $orders = array();
        $orders_aux = DB::table('orders')
        ->select(
            'users.id',
            'users.name',
            'reservations.id as reservationId',
            'products.id as productId',
            'reservations.quantity as reservationQuantity',
            'products.name as productName',
            'products.price as productPrice',
            'products.offer as productOffer',
            DB::raw('(reservations.quantity * products.price) as reservationValueTotal')
            )
        ->join('users', 'orders.user_id', 'users.id')
        ->join('reservations', 'orders.id', 'reservations.order_id')
        ->join('products', 'reservations.product_id', 'products.id')

        
        ->where('orders.state', '=', $state)
        
        ->get();
        /*foreach($orders_aux as $order){
            array_push($orders, $order->reservations);
        }   */
        return response()->json(["orders"=>$orders_aux]);
    }
    

    public function confirmarEntrega($id){
        $order = Order::findOrFail($id);
        $order->state = 2;
        $order->save();
        return response()->json(["order"=>$order]);

    }

    public function getMyorders($id)
    {        
        $orders = Order::with(['reservations'=> function($query){
            $query->with('product');
        }])
                    ->where('user_id', $id)
                    ->get();
        return response()->json(["orders"=>$orders]);
        
    }

    public function getMyordersId($order_id)
    {        
        $orders = Order::with(['reservations'=> function($query){
            $query->with('product');
        }, 'paymentMethod', 'discountVouncher', 'domiciliary', 'user'])
                    ->where('orders.id', $order_id)
                    ->get();
        return response()->json(["orders"=>$orders]);
        
    }
    public function actualizarDomiciliario($id, $domiciliary_id){
        $order = Order::find($id);
        $order->domiciliary_id = $domiciliary_id;
        $order->save();
        return response()->json(["order"=>$order]);
    }

    public function updateStateOrder(Request $request, $id, $state){
        //return response()->json(["order"=>$request->all()]);
        
        $order = Order::find($id);
        $order->state = $state;
        $order->save();
        if($state == 1){
                foreach($request->all() as $x){
                    foreach($x['reservations'] as $y){
                        
                        $product = Product::findOrFail($y['product_id']);
                        $product->quantity -= $y['quantity'];
                        $product->save();
                        if($product->quantity == 0){
                            $publications = $product->publications;
                            foreach($publications as $publication){
                                $publication->state = 2;
                                $publication->save();
                            }
                        }
                    }
                    
                }
                
        }
        if($state == 3){
            foreach($request->all() as $x){
                
                foreach($x['reservations'] as $y){
                    $product = Product::findOrFail($y['product_id']);
                    $product->quantity += $y['quantity'];
                    $product->save();
                }
            }
        }
        return response()->json(["order"=>$order]);
    }

    public function getOrderByState($state)
    {
        $orders = DB::table('orders')
                            ->join('users', 'orders.user_id', 'users.id')
                            ->select(
                                'users.id as userId',
                                'users.name as userName',
                                'orders.id as orderId',
                                'orders.created_at as orderCreated_at',
                                'orders.state as orderState'
                                )
                            ->where('orders.state', '=', $state)
                            ->get();
        return response()->json(["orders"=>$orders]);
    }

    //reportes
    public function dasboard(){
        $totalQuantity = 0;
        //declaracion de variables para segmentar ventas por días
        $start = Carbon::now()->firstOfMonth();
        $end   = Carbon::now();
        $days  = Carbon::parse($end)->diffInDays($start);
        $data = collect();
        $totalDays = collect();
        $dataFinal = [];
        $aux2 = [];
        //


        $productos = Order::productInSales(2, "this mounte");
        
        /*foreach ($data as $key => $row) {
            $aux[$key] = $row['quantity'];
        }
        
        for($i=0; $i<=$days; $i++){
            $start = Carbon::now()->firstOfMonth();
            $aux2 = [];
            $aux3 = [];
            $data[$i] = Order::getSalesQuantityByDay($start->add(new DateInterval(('P'.strval($i).'D')))->format('Y-m-d'));

            foreach ($data[$i] as $key => $row) {
                $aux2[$key] = $row['quantity'];
            }
            foreach ($data[$i] as $key => $row) {
                $aux3[$key] = $row['price'];
            }
            $totalQuantity = 0;
            $totalPrice = 0;
            for($j=0; $j<count($aux2);$j++){
                $totalQuantity += $aux2[$j];
            }
            for($k=0; $k<count($aux3);$k++){
                $totalPrice += $aux3[$k];
            }

            $start = Carbon::now()->firstOfMonth();
            $totalDays[$i] = $start->add(new DateInterval(('P'.strval($i).'D')))->format('d/M');
            array_push($dataFinal, ['date'=> $totalDays[$i], 'sales' => $data[$i], 'totalQuantity' => $totalQuantity, 'totalPrice' => $totalPrice]);
        }*/
        for($i=0; $i<=$days; $i++){
            $start = Carbon::now()->firstOfMonth();
            $data[$i] = Order::getSale($start->add(new DateInterval(('P'.strval($i).'D')))->format('Y-m-d'));
            $start = Carbon::now()->firstOfMonth();
            $totalDays[$i] = $start->add(new DateInterval(('P'.strval($i).'D')))->format('d/M');
            array_push($dataFinal, ['date'=> $totalDays[$i], 'sales' => $data[$i]]);
            //array_push($dataFinal, $data[$i]);
        }
        return response()->json(["orders"=>$dataFinal]);
    }
    
    public function getOrder($id){
        $x=Order::with('user')->where('id', $id)->get();
        return response()->json(["orders"=>$x]);

    }
}
