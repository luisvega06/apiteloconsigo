<?php

namespace App\Http\Controllers;

use App\Product;
use Illuminate\Http\Request;
use DB;

class ProductController extends Controller
{

    public function __construct()
    {
        $this->middleware('jwt.auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $products = Product::where('state', '=', 1)->orderBy('created_at', 'desc')->get();
        return response()->json(["products"=>$products]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()    
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $products = Product::create($request->input());
        return response()->json(["products"=>$products]);
    } 

    public function producto($id)
    {
        $products = DB::table('products')
        ->join('categories', 'products.category_id','categories.id')
        ->select(
            'categories.id as categoryId',
            'categories.name as categoryName',
            'products.created_at as productCreated_at',
            'products.description as productDescription',
            'products.id as productId',
            'products.picture as productPicture',
            'products.price as productPrice',
            'products.offer as productOffer',
            'products.quantity as productQuantity',
            'products.state as productState',
            'products.title as productTitle'
            )
        ->where('categories.id', $id)
        ->get();
        return response()->json(["products" => $products]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $product = Product::find($id);
        return response()->json(["product"=>$product]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function edit(Product $product)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $product = Product::find($id);
        $product->update($request->input());
        return response()->json(["product"=>$product]);
    }

    /**
     * Remove the specified resource from storage.
     * 
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $product = Product::findOrFail($id);
            $product->state = 0;
            return response()->json(["msj"=>"el producto fue eliminado del mostrador con exito"]);
        } catch (\Exception $exception) {
            return response()->json(["msj"=>"No existe el producto que quiere eliminar"]);   
        }
    }

    public function updateProduct(Request $request, $id)
    {
        $product = Product::find($id);
        $product->update($request->input());
        return response()->json(["product"=>$product]);
    }
}
