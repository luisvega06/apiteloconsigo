<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $fillable = [
        'name', 
        'quantity',
        'price', 
        'offer',
        'picture', 
        'title', 
        'description', 
        'category_id',
        'state'
    ]; 

    public function category(){
        return $this->belongsTo('App\Category');
    }
    public function publications(){
        return $this->hasMany('App\Publication');
    }
    public function reservations(){
        return $this->hasMany('App\Reservation');
    }
    public function orders(){
        return $this->hasMany('App\Order');
    }
}
