<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 
        'email', 
        'phone', 
        'direction',  
        'password', 
        'state', 
        'rol_id',
        'department_id',
        'municipality_id',
        'user_id',
        'category_id'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
         'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function publications(){
        return $this->hasMany('App\Publication');
    }

    public function users(){
        return $this->hasMany('App\User');
    }

    public function category(){
        return $this->belongsTo('App\Category');
    }

    public function user(){
        return $this->belongsTo('App\User');
    }

    public function reservations(){
        return $this->hasMany('App\Reservation');
    }
    public function orders(){
        return $this->hasMany('App\Order', 'user_id', 'id');
    }
    public function ordersDomiciliary(){
        return $this->hasMany('App\Order', 'domiciliary_id', 'id');
    }
    public function municipality(){
        return $this->belongsTo('App\Municipality');
    }


    
}
