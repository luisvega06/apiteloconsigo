<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::apiResource('users', 'UserController');
Route::post('user/update/{id}', 'UserController@update');
Route::get('get-users/{id}', 'UserController@getUsers');
Route::get('get-leaders', 'UserController@getLeaders');
Route::apiResource('categories', 'CategoryController');
Route::apiResource('publications', 'PublicationController');
Route::apiResource('products', 'ProductController');
Route::apiResource('reservations', 'ReservationController');
Route::get('reservation/my-reservations/{id}', 'ReservationController@getMyReservation');
Route::post('reservation/update-quantity/{id}/{quantity}', 'ReservationController@updateQuantity');
Route::get('reservation/{id}', 'ReservationController@eliminar');
Route::get('orders/state/{state}', 'OrderController@getOrdersState');
Route::get('orders/get-order-user/{id}', 'OrderController@getOrdersUser');
Route::get('reservation/my-orders/{id}', 'ReservationController@getMyReservations');

Route::post('reservation/order', 'ReservationController@confirmarPedido');
Route::apiResource('orders', 'OrderController');
Route::apiResource('discount_vounchers', 'DiscountVouncherController');
Route::apiResource('payment_methods', 'PaymentMethodController');
Route::apiResource('domiciliary', 'DomiciliaryController');
Route::get('product/{id}','ProductController@producto');
Route::post("/auth/login", "AuthController@login");

Route::get('discount-vouncher/{id}', 'DiscountVouncherController@eliminar');
Route::get('orders/my-orders/{id}', 'OrderController@getMyorders');
Route::get('orders/order/{id}', 'OrderController@getMyordersId');


Route::get('orders/domiciliary/{id}/{domiciliary_id}', 'OrderController@actualizarDomiciliario');
Route::post('orders/update-state-order/{id}/{state}', 'OrderController@updateStateOrder');
Route::get('orders/get-order-by-state/{state}', 'OrderController@getOrderByState');
Route::post("discount-vouncher/actualizar/{id}/{state}", "DiscountVouncherController@actualizar");

Route::post("product-update/{id}","ProductController@updateProduct");

//reportes de pedidos
Route::get('order/dasboard/', 'OrderController@dasboard');

Route::apiResource('department', 'DepartmentController');
Route::get('municipality/{department_id}', 'MunicipalityController@getMunicipality');

//domiciliarios
Route::get('domiciliario/order/{state}', 'DomiciliaryController@getOrders');
Route::post("domiciliary/actualizar/{id}/{state}", "DomiciliaryController@actualizar");

Route::get('domiciliario/my-orders/{id}/{state}', 'DomiciliaryController@getMyOrders');
Route::get('order/getOrder/{id}', 'OrderController@getOrder');
Route::get('order/updateState/{id}/{state}', 'OrderController@updateState');
Route::get('publication/updateState/{id}/{state}', 'PublicationController@updateState');
Route::get('publicationsAdmin', 'PublicationController@indexAdmin');


